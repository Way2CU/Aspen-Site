/**
 * Main JavaScript
 * Aspen Site
 *
 * Copyright (c) 2024. by Way2CU, http://way2cu.com
 * Authors: Mladen Mijatov, Vladimir Anusic
 */

// create or use existing site scope
var Site = Site || {};


/**
 * Check if site is being displayed on mobile.
 * @return boolean
 */
Site.is_mobile = function() {
	let element = document.querySelector('meta[name=is-mobile]');
	return element.content == '1';
};

/**
 * Open contact form in Dialog
 *
 * @param object event
 */
Site.handle_show_dialog_click = function(event) {
	event.preventDefault();
	Site.dialog_form.open();
}

/**
 * Function called when document and images have been completely loaded.
 */
Site.on_load = function() {
	if (Site.is_mobile())
		Site.mobile_menu = new Caracal.MobileMenu();

	var submit_button = document.querySelector('button.submit');

	// dialog which contains form
	Site.dialog_form = new Caracal.Dialog();
	Site.dialog_form
		.set_content_from_dom('div.form_container')
		.add_class('dialog_form')
		.add_control(submit_button); //move form submit button alongside dialog close button

	submit_button.addEventListener('click', function(event) {
		event.preventDefault();
		Caracal.ContactForm.list[0]._form.submit();
	});

	// open form in dialog
	Site.show_dialog_button = document.querySelectorAll('a.contact_us');
	Site.show_dialog_button.forEach((b) => b.addEventListener('click', Site.handle_show_dialog_click));
};


// connect document `load` event with handler function
window.addEventListener('load', Site.on_load);
