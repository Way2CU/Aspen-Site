/**
 * Floating Bubbles
 * Aspen Site
 *
 * Copyright (c) 2020. by Way2CU, http://way2cu.com
 * Authors: Mladen Mijatov, Vladimir Anusic
 */

// create or use existing site scope
var Site = Site || new Object();


Site.Bubble = function(animation, pos_x, pos_y, radius, time, distance,  text) {
	var self = this;

	self.start_position_x = null;
	self.radius = 0;
	self.image_x = 0;
	self.image_y = 0;
	self.container = null;
	self.distance = null;
	self.time = null;

	/**
	 * Complete object initialization.
	 */
	self._init = function() {
		self.start_position_x = pos_x;
		self.image_x = pos_x;
		self.image_y = pos_y;
		self.radius = radius;
		self.diameter = radius * 2;
		self.time = time;
		self.distance = distance;
		self.container = document.createElement('span');

		self.container.style.width = self.diameter.toString() + 'px';
		self.container.style.height = self.diameter.toString() + 'px';
		self.container.style.left = (pos_x + radius).toString() + 'px';
		self.container.style.top = (pos_y + radius).toString() + 'px';

		if (text)
			self.container.innerHTML = text;

		animation.container.append(self.container);
	};

	/**
	 * Update element position and status.
	 *
	 * @param integer frame_time
	 */
	self.update = function(frame_time) {
		var move_by = (self.distance * frame_time) / self.time;

		self.image_y = Math.round(self.image_y - move_by);
		self.container.style.left = self.image_x.toString() + 'px';
		self.container.style.top = self.image_y.toString() + 'px';

		if (self.container.offsetTop + self.diameter < 0)
			animation.notify_out_of_bounds(self);
	};

	/**
	 * Update bubble coordinates to specified.
	 *
	 * @param integer pos_x
	 * @param integer pos_y
	 */
	self.move = function(pos_x, pos_y) {
		self.image_x = pos_x + self.radius;
		self.image_y = pos_y + self.radius;

		self.container.style.left = self.image_x.toString() + 'px';
		self.container.style.top = self.image_y.toString() + 'px';
	};

	// finalize object
	self._init();
}


Site.Foam = function(count) {
	var self = this;

	self.enabled = false;
	self.items = new Array();
	self.frame_timestamp = null;
	self.start_time = 0;
	self.container = null;

	/**
	 * Complete object initialization.
	 */
	self._init = function() {
		var text_bubbles = [
			'Beauty', 'Rejuvenation', 'Antiageing', 'Glamour',
			'Health', 'Energizing', 'Regeneration'
		];

		self.container = document.querySelector('div.foam');
		self.frame_timestamp = performance.now();

		var width = self.container.offsetWidth;
		var height = self.container.offsetHeight;

		for (var i=0; i < count; i++) {
			var pos_x = Math.round(Math.random() * width);
			var pos_y = Math.round(Math.random() * height);
			var time = (Math.random() * 15) + 5;

			if (i < text_bubbles.length) {
				var radius = Math.round(50 + (Math.random() * 10));
				var text = text_bubbles[i]
			} else {
				var radius = Math.round(20 + (Math.random() * 20));
				var text = null;
			}

			self.items.push(new Site.Bubble(self, pos_x, pos_y, radius, time * 1000, height, text));
		}
	};

	/**
	 * Function called by each individual bubble when it leaves bounds.
	 *
	 * @param object bubble
	 */
	self.notify_out_of_bounds = function(bubble) {
		var width = self.container.offsetWidth;
		var pos_x = Math.round(Math.random() * width);
		bubble.move(pos_x, self.container.offsetHeight);
	};

	/**
	 * Handle animation frame.
	 */
	self.animate = function() {
		var now = performance.now();
		var frame_time = now - self.frame_timestamp;
		for (var i=0, count=self.items.length; i<count; i++)
			self.items[i].update(frame_time);
		self.frame_timestamp = now;

		if (self.enabled)
			window.requestAnimationFrame(self.animate);
	};

	/**
	 * Start animation cycle.
	 */
	self.start = function() {
		self.enabled = true;
		window.requestAnimationFrame(self.animate);
	};

	/**
	 * Stop animation cycle.
	 */
	self.stop = function() {
		self.enabled = false;
	};

	// finalize object
	self._init();
}


window.addEventListener('load', function() {
	Site.bubbles = new Site.Foam(30);
	Site.bubbles.start();
});
